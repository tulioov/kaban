
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

/**
 * <p>O propósito desta interface é estabalecer os estados de um Kanban
 * 
 * <p>Há apenas 2 estados: Ativo e Inativo (mapeados em true e false)
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Nomeavel
 */
public interface EstadoKanban {
    
    public boolean isAtivo(Nomeavel obj);
    
    public void setInativo(Nomeavel obj);
}
