
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

/**
 * <p>O propósito desta interface é estabelecer o estado de um Tarefa
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Nomeavel
 * @see     EstadosTarefa
 */
public interface EstadoTarefa {
    
    /**
     * @param   kanban o Kanban
     * @param   tarefa a Tarefa
     * @param   estado o <i>novo</i> estado
     * @return  true se a operação teve sucesso
     */
    public boolean setEstado(Nomeavel kanban, Nomeavel tarefa, EstadosTarefa estado);
    
    /**
     * @param   kanban o Kanban
     * @param   tarefa a Tarefa
     * @return  o estado da Tarefa
     */
    public EstadosTarefa getEstado(Nomeavel kanban, Nomeavel tarefa);
}
