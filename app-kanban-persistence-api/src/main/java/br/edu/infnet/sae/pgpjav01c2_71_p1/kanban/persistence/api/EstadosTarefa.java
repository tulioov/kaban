
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

/**
 * O propósito deste tipo é "ammarrar" os estados de uma Tarefa
 * 
 * @author Túlio, Roberval, Lucas, Rafael
 */
public enum EstadosTarefa {

	A_FAZER(1, "A_FAZER"), 
	EM_ANDAMENTO(2 , "EM_ANDAMENTO"), 
	EM_IMPEDIMENTO(3 , "EM_IMPEDIMENTO"), 
	PRONTA(4, "PRONTA"), 
	CANCELADA(5, "CANCELADA"), 
	ERRO_DE_STATUS(6, "ERRO_DE_STATUS");

	private int id;
	private String value;

	private EstadosTarefa(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public String getValue() {
		return value;
	}

}
