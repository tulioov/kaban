
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

/**
 * <p>O propósito desta interface é criar um tipo para
 * <i>identificar</i> Kanbans, Tarefas e Usuários
 * 
 *  <p>O <tt>id</tt> é o identificador do objeto.
 *  Recomenda-se que esteja/seja implementado via o método <tt>hashCode()</tt>
 * 
 *  <p>O identificador é um <tt>long</tt> pois este é o tipo mais geral.
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Object
 */
public interface Identificavel {

    public long getId();
    
    public void setId(long id);
    
}
