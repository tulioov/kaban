
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

import java.util.Date;

/**
 * <p>O propósito desta interface é criar um tipo (<i>Value Object</i>) para
 * comunicar/transferir o <i>evento</i> (informações) ao Log.
 * 
 * <p>A indentificação para o <tt>Log</tt> provém (indiretamente) do <tt>Kanban</tt>
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Nomeavel
 * @see     PessoaFisica
 */
public interface LogVO {
    
    public Date getDataHora();

    public void setDataHora(Date dataHora);
    
    public Nomeavel getKanban();
    
    public void setKanban(Nomeavel kanban);
    
    public Nomeavel getTarefa();
    
    public void setTarefa(Nomeavel tarefa);
    
    public PessoaFisica getUsuario();
    
    public void setUsuario(PessoaFisica usuario);  
    
    public EstadosTarefa getEstado();
    
    public void setTarefaEstado(EstadosTarefa estado);
}
