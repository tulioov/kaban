
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;


/**
 * <p>O propósito desta interface é criar um tipo para
 * <i>nomear</i> Kanbans, Tarefas e Usuários
 * 
 *  <p>O <tt>nome</tt> é o nome do objeto. O tipo <tt>String</tt> é usado na representação <i>interna</i>.
 * 
 *  <p>Esta interface pressupõe (o uso) nomes <i>corretos</i>.
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Object
 */
public interface Nomeavel extends Identificavel, Comparable {
    
    public String getNome();
    
    public void setNome(String nome);
        
}
