
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

/**
 * <p>O propósito desta interface é criar um tipo para
 * representar <tt>Usuarios</tt> dos <tt>Kanbans</tt>
 * 
 *  <p>O <tt>CPF</tt> deve ser empregado como identificador (<tt>Identificavel</tt>) do <tt>Usuario</tt>.
 * 
 *  <p>Esta interface pressupõe (o uso) CPF <i>correto</i>.
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Nomeavel
 */
public interface PessoaFisica extends Nomeavel {
    
    public long getCPF();
    
    public void setCPF(long cpf);
    
}
