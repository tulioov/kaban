
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api;

import java.util.List;

/**
 * <p>O propósito desta interface é criar um tipo para
 * as operações de armazenamento: criação, atualização e listas de objetos.
 * 
 * <p>Em geral T será <tt>Nomeavel</tt> (consequentemente <tt>Identificavel</tt>)
 * 
 * <p><tt>Kanbans</tt> e <tt>Logs</tt> podem ser armazenados diretamente sem identificadores adicionais. 
 * Contudo, <tt>Tarefas</tt> e <tt>Usuarios</tt> necessitam do <tt>Kanban</tt> do qual são parte. 
 * 
 * @author  Túlio, Roberval, Lucas, Rafael
 * @see     Identificavel
 * @see     Nomeavel
 * @param   <T> tipo do objeto a ser transferido 
 */
public interface StoreApi<T> {

    /**
     *
     * @param   obj o objeto a ser criado
     * @param   ids dos objetos (Identificaveis) que determinam qual o objeto a ser atualizado (dependendo do objeto esses identificaddores podem ser desnecessários)
     * @return  true se a operação teve sucesso
     */
    public boolean create(T obj, Identificavel ... ids);
    
    /**
     *
     * @param obj o objeto a ser atualizado
     * @param ids dos objetos (Identificaveis) que determinam qual o objeto a ser atualizado (dependendo do objeto esses identificaddores podem ser desnecessários)
     */
    public void update(T obj, Identificavel ... ids);
    
    /**
     *
     * @param   ids dos objetos (Identificaveis) que determinam a lista desejada (dependendo do objeto esses identificaddores podem ser desnecessários)
     * @return  a lista dos objetos (Kanbans, Tarefas, Usuários)
     */
    public List<T> list(Identificavel ... ids);
    
    
}
