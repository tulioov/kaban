package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl;

interface DB {
  public static final String DDL_KANBAN = "create table if not exists kanban (id int not null, nome varchar(80) not null, estado boolean not null default TRUE, primary key (id))";
  
  public static final String DDL_USUARIO = "create table if not exists usuario (id_kanban int not null, cpf long not null, nome varchar(80) not null, primary key (id_kanban, cpf), constraint usuario_fk foreign key (id_kanban) references kanban(id))";
  
  public static final String DDL_TAREFA = "create table if not exists tarefa (id_kanban int not null, id int not null, nome varchar(80) not null, estado varchar(30) not null default 'a_fazer', primary key (id_kanban, id), constraint tarefa_fk foreign key (id_kanban) references kanban(id), constraint tarefa_estado_chk check estado in ('a_fazer', 'em_andamento', 'em_impedimento', 'pronta', 'cancelada'))";
  
  public static final String DDL_LOG = "create table if not exists log (id integer not null auto_increment, id_kanban int not null, ts timestamp default current_timestamp, nome_kanban varchar(80) not null, nome_tarefa varchar(80) not null, nome_usuario varchar(80), cpf long not null, estado varchar(30) not null, primary key (id), constraint log_estado_chk check estado in ('a_fazer', 'em_andamento', 'em_impedimento', 'pronta', 'cancelada'))";
  
  public static final String DML_KANBAN_CREATE = "insert into kanban (id, nome) values (?, ?)";
  
  public static final String DML_KANBAN_SET_INATIVO = "update kanban set estado = FALSE where id = ?";
  
  public static final String SQL_KANBAN_GET_ESTADO = "select estado from kanban where id = ?";
  
  public static final String SQL_KANBAN_LIST = "select nome from kanban";
  
  public static final String DML_TAREFA_CREATE = "insert into tarefa (id_kanban, id, nome) values (?, ?, ?)";
  
  public static final String DML_TAREFA_SET_ESTADO = "update tarefa set estado = ? where id_kanban = ? and id = ?";
  
  public static final String SQL_TAREFA_GET_ESTADO = "select estado from tarefa where id_kanban = ? and id = ?";
  
  public static final String SQL_TAREFA_LISTA = "select nome from tarefa where id_kanban = ?";
  
  public static final String DML_USUARIO_CREATE = "insert into usuario (id_kanban, cpf, nome) values (?, ?, ?)";
  
  public static final String DML_USUARIO_CHANGE_NOME = "update usuario set nome = ? where id_kanban = ? and cpf = ?";
  
  public static final String SQL_USUARIO_LISTA = "select nome, cpf from usuario where id_kanban = ?";
  
  public static final String DML_LOG_CREATE = "insert into log (id_kanban, nome_kanban, nome_tarefa, estado, nome_usuario, cpf) values (?, ?, ?, ?, ?, ?)";
  
  public static final String SQL_LOG_LISTA = "select ts, nome_kanban, nome_tarefa, estado, nome_usuario, cpf from log where id_kanban = ?";
}


/* Location:              C:\Users\tulio\Downloads\kanban-1.0.0\kanban-1.0.0\app-kanban-persistence-impl-1.0.0.jar!\br\edu\infnet\sae\pgpjav01c2_71_p1\kanban\persistence\impl\DB.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.0.2
 */