/*     */ package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl;
/*     */ 
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadoKanban;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Identificavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.StoreApi;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class KanbanStoreService
/*     */   extends PersistenceServices
/*     */   implements StoreApi<Nomeavel>, EstadoKanban
/*     */ {
/*     */   public boolean isAtivo(Nomeavel kanban) {
/*     */     
/*  51 */     try { c = getConnection(); throwable = null; 
/*  52 */       try { c.setAutoCommit(true);
/*  53 */         ps = c.prepareStatement("select estado from kanban where id = ?"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */         throw throwable1; }
/*     */       finally
/*  58 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/*  59 */     { return false; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void setInativo(Nomeavel kanban) {
/*     */     
/*  71 */     try { c = getConnection(); throwable = null; 
/*  72 */       try { c.setAutoCommit(true);
/*  73 */         ps = c.prepareStatement("update kanban set estado = FALSE where id = ?"); throwable1 = null; 
/*  74 */         try { ps.setInt(1, (int)kanban.getId());
/*  75 */           ps.executeUpdate(); } catch (Throwable throwable2) { throwable1 = throwable2 = null; throw throwable2; }
/*  76 */         finally { if (ps != null) if (throwable1 != null) { try { ps.close(); } catch (Throwable throwable2) { throwable1.addSuppressed(throwable2); }  } else { ps.close(); }   }  } catch (Throwable throwable1) { throwable = throwable1 = null; throw throwable1; }
/*  77 */       finally { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException sQLException) {}
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean create(Nomeavel kanban, Identificavel... ids) {
/*     */     
/*  93 */     try { c = getConnection(); throwable = null; 
/*  94 */       try { c.setAutoCommit(true);
/*  95 */         ps = c.prepareStatement("insert into kanban (id, nome) values (?, ?)"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 101 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 102 */     { return false; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public List<Nomeavel> list(Identificavel... ids) {
/*     */     
/* 116 */     try { c = getConnection(); throwable = null; 
/* 117 */       try { c.setAutoCommit(true);
/* 118 */         ps = c.prepareStatement("select nome from kanban"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 160 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 161 */     { return new ArrayList(); }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/* 175 */   public void update(Nomeavel obj, Identificavel... ids) { throw new UnsupportedOperationException("Not supported"); }
/*     */ }


/* Location:              C:\Users\tulio\Downloads\kanban-1.0.0\kanban-1.0.0\app-kanban-persistence-impl-1.0.0.jar!\br\edu\infnet\sae\pgpjav01c2_71_p1\kanban\persistence\impl\KanbanStoreService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.0.2
 */