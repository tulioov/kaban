/*     */ package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl;
/*     */ 
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Identificavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.LogVO;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.PessoaFisica;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.StoreApi;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Date;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class LogStoreService
/*     */   extends PersistenceServices
/*     */   implements StoreApi<LogVO>
/*     */ {
/*     */   public boolean create(LogVO log, Identificavel... ids) {
/*     */     
/*  37 */     try { c = getConnection(); throwable = null; 
/*  38 */       try { c.setAutoCommit(true);
/*  39 */         ps = c.prepareStatement("insert into log (id_kanban, nome_kanban, nome_tarefa, estado, nome_usuario, cpf) values (?, ?, ?, ?, ?, ?)"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/*  49 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/*  50 */     { return false; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*  63 */   public void update(LogVO obj, Identificavel... ids) { throw new UnsupportedOperationException("Not supported"); }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public List<LogVO> list(Identificavel... ids) {
/*     */     
/*  74 */     try { c = getConnection(); throwable = null; 
/*  75 */       try { c.setAutoCommit(true);
/*  76 */         ps = c.prepareStatement("select ts, nome_kanban, nome_tarefa, estado, nome_usuario, cpf from log where id_kanban = ?"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 231 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 232 */     { return new ArrayList(); }
/*     */   
/*     */   }
/*     */ }


/* Location:              C:\Users\tulio\Downloads\kanban-1.0.0\kanban-1.0.0\app-kanban-persistence-impl-1.0.0.jar!\br\edu\infnet\sae\pgpjav01c2_71_p1\kanban\persistence\impl\LogStoreService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.0.2
 */