/*    */ package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl;
/*    */ 
/*    */ import java.io.FileInputStream;
/*    */ import java.io.IOException;
/*    */ import java.io.InputStream;
/*    */ import java.sql.Connection;
/*    */ import java.sql.DriverManager;
/*    */ import java.sql.PreparedStatement;
/*    */ import java.sql.SQLException;
/*    */ import java.util.Properties;
/*    */ import java.util.logging.Logger;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ abstract class PersistenceServices
/*    */ {
/* 21 */   private static final Properties DB = new Properties(); private static String DB_CONNECTION; private static String DB_USER;
/*    */   private static String DB_PASSWORD;
/* 23 */   static Logger log = Logger.getAnonymousLogger();
/*    */   static  {
/*    */     
/* 26 */     try { is = PersistenceServices.class.getResourceAsStream("/db.properties"); throwable = null; 
/* 27 */       try { setup(is); } catch (Throwable throwable1) { throwable = throwable1 = null; throw throwable1; }
/* 28 */       finally { if (is != null) if (throwable != null) { try { is.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { is.close(); }   }  } catch (IOException|ClassNotFoundException|NullPointerException ex)
/* 29 */     { ex.printStackTrace(); 
/* 30 */       try { is = new FileInputStream("src/test/resources/db.properties"); throwable = null; 
/* 31 */         try { setup(is); } catch (Throwable throwable1) { throwable = throwable1 = null; throw throwable1; }
/* 32 */         finally { if (is != null) if (throwable != null) { try { is.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { is.close(); }   }  } catch (IOException|ClassNotFoundException|NullPointerException ex1)
/* 33 */       { ex.printStackTrace();
/* 34 */         error("db config error " + ex.getMessage()); }
/*    */        }
/*    */   
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   private static void setup(InputStream is) throws IOException, ClassNotFoundException, NullPointerException { // Byte code:
/*    */     //   0: getstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB : Ljava/util/Properties;
/*    */     //   3: aload_0
/*    */     //   4: invokevirtual load : (Ljava/io/InputStream;)V
/*    */     //   7: getstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB : Ljava/util/Properties;
/*    */     //   10: ldc 'driver'
/*    */     //   12: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
/*    */     //   15: checkcast java/lang/String
/*    */     //   18: invokestatic forName : (Ljava/lang/String;)Ljava/lang/Class;
/*    */     //   21: pop
/*    */     //   22: getstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB : Ljava/util/Properties;
/*    */     //   25: ldc 'url'
/*    */     //   27: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
/*    */     //   30: checkcast java/lang/String
/*    */     //   33: putstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB_CONNECTION : Ljava/lang/String;
/*    */     //   36: getstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB : Ljava/util/Properties;
/*    */     //   39: ldc 'user'
/*    */     //   41: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
/*    */     //   44: checkcast java/lang/String
/*    */     //   47: putstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB_USER : Ljava/lang/String;
/*    */     //   50: getstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB : Ljava/util/Properties;
/*    */     //   53: ldc 'password'
/*    */     //   55: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
/*    */     //   58: checkcast java/lang/String
/*    */     //   61: putstatic br/edu/infnet/sae/pgpjav01c2_71_p1/kanban/persistence/impl/PersistenceServices.DB_PASSWORD : Ljava/lang/String;
/*    */     //   64: iconst_4
/*    */     //   65: anewarray java/lang/String
/*    */     //   68: dup
/*    */     //   69: iconst_0
/*    */     //   70: ldc 'create table if not exists kanban (id int not null, nome varchar(80) not null, estado boolean not null default TRUE, primary key (id))'
/*    */     //   72: aastore
/*    */     //   73: dup
/*    */     //   74: iconst_1
/*    */     //   75: ldc 'create table if not exists tarefa (id_kanban int not null, id int not null, nome varchar(80) not null, estado varchar(30) not null default 'a_fazer', primary key (id_kanban, id), constraint tarefa_fk foreign key (id_kanban) references kanban(id), constraint tarefa_estado_chk check estado in ('a_fazer', 'em_andamento', 'em_impedimento', 'pronta', 'cancelada'))'
/*    */     //   77: aastore
/*    */     //   78: dup
/*    */     //   79: iconst_2
/*    */     //   80: ldc 'create table if not exists usuario (id_kanban int not null, cpf long not null, nome varchar(80) not null, primary key (id_kanban, cpf), constraint usuario_fk foreign key (id_kanban) references kanban(id))'
/*    */     //   82: aastore
/*    */     //   83: dup
/*    */     //   84: iconst_3
/*    */     //   85: ldc 'create table if not exists log (id integer not null auto_increment, id_kanban int not null, ts timestamp default current_timestamp, nome_kanban varchar(80) not null, nome_tarefa varchar(80) not null, nome_usuario varchar(80), cpf long not null, estado varchar(30) not null, primary key (id), constraint log_estado_chk check estado in ('a_fazer', 'em_andamento', 'em_impedimento', 'pronta', 'cancelada'))'
/*    */     //   87: aastore
/*    */     //   88: astore_1
/*    */     //   89: aload_1
/*    */     //   90: arraylength
/*    */     //   91: istore_2
/*    */     //   92: iconst_0
/*    */     //   93: istore_3
/*    */     //   94: iload_3
/*    */     //   95: iload_2
/*    */     //   96: if_icmpge -> 115
/*    */     //   99: aload_1
/*    */     //   100: iload_3
/*    */     //   101: aaload
/*    */     //   102: astore #4
/*    */     //   104: aload #4
/*    */     //   106: invokestatic exec : (Ljava/lang/String;)V
/*    */     //   109: iinc #3, 1
/*    */     //   112: goto -> 94
/*    */     //   115: return
/*    */     // Line number table:
/*    */     //   Java source line number -> byte code offset
/*    */     //   #40	-> 0
/*    */     //   #41	-> 7
/*    */     //   #42	-> 22
/*    */     //   #43	-> 36
/*    */     //   #44	-> 50
/*    */     //   #45	-> 64
/*    */     //   #46	-> 104
/*    */     //   #45	-> 109
/*    */     //   #48	-> 115
/*    */     // Local variable table:
/*    */     //   start	length	slot	name	descriptor
/*    */     //   104	5	4	ddl	Ljava/lang/String;
/*    */     //   0	116	0	is	Ljava/io/InputStream; }
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   static Connection getConnection() {
/* 51 */     dbConnection = null;
/*    */     try {
/* 53 */       return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
/* 54 */     } catch (SQLException e) {
/* 55 */       error(e.getMessage());
/*    */       
/* 57 */       return dbConnection;
/*    */     } 
/*    */   } static void exec(String cmd) {
/*    */     
/* 61 */     try { c = getConnection(); throwable = null; 
/* 62 */       try { c.setAutoCommit(true);
/* 63 */         ps = c.prepareStatement(cmd); throwable1 = null; 
/* 64 */         try { ps.executeUpdate(); } catch (Throwable throwable2) { throwable1 = throwable2 = null; throw throwable2; }
/* 65 */         finally { if (ps != null) if (throwable1 != null) { try { ps.close(); } catch (Throwable throwable2) { throwable1.addSuppressed(throwable2); }  } else { ps.close(); }   }  } catch (Throwable throwable1) { throwable = throwable1 = null; throw throwable1; }
/* 66 */       finally { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 67 */     { error(ex.getMessage()); }
/*    */   
/*    */   }
/*    */   
/*    */   static void error(String message) {
/* 72 */     System.err.println(message);
/* 73 */     System.exit(1);
/*    */   }
/*    */ }


/* Location:              C:\Users\tulio\Downloads\kanban-1.0.0\kanban-1.0.0\app-kanban-persistence-impl-1.0.0.jar!\br\edu\infnet\sae\pgpjav01c2_71_p1\kanban\persistence\impl\PersistenceServices.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.0.2
 */