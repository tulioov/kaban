/*     */ package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl;
/*     */ 
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadoTarefa;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Identificavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.StoreApi;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class TarefaStoreService
/*     */   extends PersistenceServices
/*     */   implements StoreApi<Nomeavel>, EstadoTarefa
/*     */ {
/*     */   public boolean create(Nomeavel tarefa, Identificavel... ids) {
/*     */     
/*  55 */     try { c = getConnection(); throwable = null; 
/*  56 */       try { c.setAutoCommit(true);
/*  57 */         ps = c.prepareStatement("insert into tarefa (id_kanban, id, nome) values (?, ?, ?)"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/*  64 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/*  65 */     { return false; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*  79 */   public void update(Nomeavel obj, Identificavel... ids) { throw new UnsupportedOperationException("Not supported"); }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public List<Nomeavel> list(Identificavel... ids) {
/*     */     
/*  92 */     try { c = getConnection(); throwable = null; 
/*  93 */       try { c.setAutoCommit(true);
/*  94 */         ps = c.prepareStatement("select nome from tarefa where id_kanban = ?"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 138 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 139 */     { return null; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public boolean setEstado(Nomeavel kanban, Nomeavel tarefa, EstadosTarefa estado) {
/*     */     
/* 155 */     try { c = getConnection(); throwable = null; 
/* 156 */       try { c.setAutoCommit(true);
/* 157 */         ps = c.prepareStatement("update tarefa set estado = ? where id_kanban = ? and id = ?"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 164 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 165 */     { return false; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public EstadosTarefa getEstado(Nomeavel kanban, Nomeavel tarefa) {
/*     */     
/* 181 */     try { c = getConnection(); throwable = null; 
/* 182 */       try { c.setAutoCommit(true);
/* 183 */         ps = c.prepareStatement("select estado from tarefa where id_kanban = ? and id = ?"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 190 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 191 */     { return null; }
/*     */   
/*     */   }
/*     */ }


/* Location:              C:\Users\tulio\Downloads\kanban-1.0.0\kanban-1.0.0\app-kanban-persistence-impl-1.0.0.jar!\br\edu\infnet\sae\pgpjav01c2_71_p1\kanban\persistence\impl\TarefaStoreService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.0.2
 */