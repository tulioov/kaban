/*     */ package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl;
/*     */ 
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Identificavel;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.PessoaFisica;
/*     */ import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.StoreApi;
/*     */ import java.sql.Connection;
/*     */ import java.sql.PreparedStatement;
/*     */ import java.sql.ResultSet;
/*     */ import java.sql.SQLException;
/*     */ import java.util.ArrayList;
/*     */ import java.util.List;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class UsuarioStoreService
/*     */   extends Object
/*     */   implements StoreApi<PessoaFisica>
/*     */ {
/*     */   public boolean create(PessoaFisica usuario, Identificavel... ids) {
/*     */     
/*  36 */     try { c = PersistenceServices.getConnection(); throwable = null; 
/*  37 */       try { c.setAutoCommit(true);
/*  38 */         ps = c.prepareStatement("insert into usuario (id_kanban, cpf, nome) values (?, ?, ?)"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/*  45 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/*  46 */     { return false; }
/*     */   
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public void update(PessoaFisica usuario, Identificavel... ids) {
/*     */     
/*  60 */     try { c = PersistenceServices.getConnection(); throwable = null; 
/*  61 */       try { c.setAutoCommit(true);
/*  62 */         ps = c.prepareStatement("update usuario set nome = ? where id_kanban = ? and cpf = ?"); throwable1 = null; 
/*  63 */         try { ps.setString(1, usuario.getNome());
/*  64 */           ps.setInt(2, (int)ids[0].getId());
/*  65 */           ps.setLong(3, usuario.getCPF());
/*  66 */           ps.executeUpdate(); } catch (Throwable throwable2) { throwable1 = throwable2 = null; throw throwable2; }
/*  67 */         finally { if (ps != null) if (throwable1 != null) { try { ps.close(); } catch (Throwable throwable2) { throwable1.addSuppressed(throwable2); }  } else { ps.close(); }   }  } catch (Throwable throwable1) { throwable = throwable1 = null; throw throwable1; }
/*  68 */       finally { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException sQLException) {}
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public List<PessoaFisica> list(Identificavel... ids) {
/*     */     
/*  81 */     try { c = PersistenceServices.getConnection(); throwable = null; 
/*  82 */       try { c.setAutoCommit(true);
/*  83 */         ps = c.prepareStatement("select nome, cpf from usuario where id_kanban = ?"); throwable1 = null; }
/*     */       catch (Throwable throwable1)
/*     */       { throwable = throwable1 = null;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */         
/*     */         throw throwable1; }
/*     */       finally
/* 138 */       { if (c != null) if (throwable != null) { try { c.close(); } catch (Throwable throwable1) { throwable.addSuppressed(throwable1); }  } else { c.close(); }   }  } catch (SQLException ex)
/* 139 */     { return new ArrayList(); }
/*     */   
/*     */   }
/*     */ }


/* Location:              C:\Users\tulio\Downloads\kanban-1.0.0\kanban-1.0.0\app-kanban-persistence-impl-1.0.0.jar!\br\edu\infnet\sae\pgpjav01c2_71_p1\kanban\persistence\impl\UsuarioStoreService.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.0.2
 */