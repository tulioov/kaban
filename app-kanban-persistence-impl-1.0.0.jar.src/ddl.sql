
-- check
-- java -cp ~/h2-1.4.200.jar org.h2.tools.RunScript -url "jdbc:h2:mem:db" -user user -password pass -script ddl.sql -showResults
-- 

create table if not exists kanban (id int not null, nome varchar(80) not null, estado boolean not null default 'true', primary key (id));
create table if not exists usuario (id_kanban int not null, cpf long not null, nome varchar(80) not null, primary key (id_kanban, cpf), constraint usuario_fk foreign key (id_kanban) references kanban(id));
create table if not exists tarefa (id_kanban int not null, id int not null, nome varchar(80) not null, estado varchar(30) not null default 'a_fazer', primary key (id_kanban, id), constraint tarefa_fk foreign key (id_kanban) references kanban(id), constraint tarefa_estado_chk check estado in ('a_fazer', 'em_andamento', 'em_impedimento', 'pronta', 'cancelada'));
create table if not exists log (id integer not null auto_increment, ts timestamp default current_timestamp, nome_kanban varchar(80) not null, nome_tarefa varchar(80) not null, nome_usuario varchar(80), cpf long not null, estado varchar(30) not null, primary key (id), constraint log_estado_chk check estado in ('a_fazer', 'em_andamento', 'em_impedimento', 'pronta', 'cancelada'))
