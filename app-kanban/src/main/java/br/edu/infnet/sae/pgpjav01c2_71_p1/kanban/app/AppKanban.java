package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app;

import static java.lang.System.err;
import static java.lang.System.exit;
import static java.lang.System.out;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app._aux.Nome;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.domain.Commands;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.domain.Errors;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model.Kanban;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model.Tarefa;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model.Usuario;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.util.Log;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadoTarefa;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;

/**
 * Implementação parcial
 * 
 * @author Túlio, Roberval, Lucas, Rafael
 */
public class AppKanban {

    public static void main(String... args) {
        try {
            String kbnNome, usrNome, usrCPF, tskNome, tskStatus;
            Kanban kbn = new Kanban();
            Tarefa tsk = new Tarefa();

            switch (Commands.select(args)) {
                case KANBAN_CREATE:
                    kbnNome = args[2];
                    kbn.setNome(kbnNome);
                    kbn.persist();
                    break;
                case KANBAN_EXCLUDE:
                    kbnNome = args[2];
                    kbn.setNome(kbnNome);
                    kbn.setInativo();
                    break;
                case KANBAN_LIST:
                    out.print(kbn.toString());
                    break;
                case KANBAN_USER_CREATE:
                    kbnNome = args[1];
                    usrNome = args[3];
                    usrCPF = args[5];
                    kbn.setNome(kbnNome);
                    kbn.criarUsuario(new Usuario(new Nome(usrNome), usrCPF), kbn);
                    out.println("USER CRIADO COM SUCESS");
                    break;
                case KANBAN_USER_ALTER:
                	kbnNome = args[1];
                    usrNome = args[3];
                    usrCPF = args[5];
                    kbn.setNome(kbnNome);
                    kbn.alterarUsuario(new Usuario(new Nome(usrNome), usrCPF) , kbn);
                    break;
                case KANBAN_USER_LIST:
                    kbnNome = args[1];
                    usrCPF = args[4];
                    kbn.setNome(kbnNome);
                    out.print(kbn.listarUsuarios(new Usuario(usrCPF) , kbn));
                    break;
                case KANBAN_HELP:
                    help();
                    break;
                                    
                case TAREFA_CREATE:
                	kbnNome = args[1];
                    tskNome = args[3];
                    usrCPF = args[5];
                    kbn.setNome(kbnNome);
                    tsk.setNome(tskNome);
                    kbn.criarTsk(tsk,kbn,Long.parseLong(usrCPF));
                    
                	break;
                case TAREFA_EXCLUDE:
                	kbnNome = args[1];
                    tskNome = args[3];
                    usrCPF = args[5];
                    kbn.setNome(kbnNome);
                    tsk.setNome(tskNome);
                    kbn.setTskEstado(kbn, tsk, EstadosTarefa.CANCELADA, Long.parseLong(usrCPF));
                	break;
                case TAREFA_STATE:
                	kbnNome = args[1];
                	tskNome = args[3];
                    tskStatus = args[5];
                    usrCPF = args[7];   
                    tsk.setNome(tskNome);
                    kbn.setNome(kbnNome);
                    kbn.setTskEstado(kbn, tsk, tsk.getStatus(tskStatus), Long.parseLong(usrCPF));
                	break;
                case TAREFA_LIST:
                	kbnNome = args[1];
                	usrCPF = args[4];  
                    kbn.setNome(kbnNome);
                    out.print(kbn.listarTarefas((kbn),Long.parseLong(usrCPF)));
                	break;
                case KANBAN_LOG_LIST:
                	kbnNome = args[1];
                	usrCPF = args[4];
                	kbn.listarLog(kbnNome, usrCPF);
                	break;
   
                default:
                    error(Errors.KANBAN_COMANDO_INEXISTENTE);
            }
            exit(0);
        } catch (Throwable e) {
            if (e instanceof KanbanException) {
                error(((KanbanException)e).getErro());
            } else {
                err.println("KANBAN_BUG " + e.getMessage());
            }
            exit(1);
        }
    }

    static void help() {
        out.println("App Kanban");
        out.println(" --help (este help)");
        out.println(" --adm --kanban-create");
        out.println(" --adm --kanban-list");
        out.println(" --adm --kanban-exclude");
        out.println(" --kanban 'nome-do-kanban' --user-create 'nome-do-usuário' --cpf número-do-cpf");
        out.println(" --kanban 'nome-do-kanban' --user-change 'nome-do-usuário' --cpf número-do-cpf");
        out.println(" --kanban 'nome-do-kanban' --user-list --cpf número-do-cpf");
        out.println("--kanban ‘nome-do-kanban’ --task-create ‘nome-da-tarefa’ --cpf ‘número-do-cpf’");
        out.println("--kanban ‘nome-do-kanban’ --task-exclude ‘nome-da-tarefa’ --cpf ‘número-do-cpf’");
        out.println("--kanban ‘nome-do-kanban’ --task ‘nome-da-tarefa’ --task-state ‘estado’ --cpf ‘número-do-cpf’");
        out.println("--kanban ‘nome-do-kanban’ --task-list --cpf ‘número-do-cpf’");
      //A FAZER
        out.println("--kanban ‘nome-do-kanban’ --log-list --cpf ‘número-do-cpf’");
        
    }
    
    static void error(Errors error) {
        // ... melhorar ?
        err.print(error.toString());
    }
}
