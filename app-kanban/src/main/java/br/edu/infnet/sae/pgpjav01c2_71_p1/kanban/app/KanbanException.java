
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.domain.Errors;

/**
 *
 * @author Túlio, Roberval, Lucas, Rafael
 */
public class KanbanException extends Exception {

    private final Errors erro;
    private final String[] msgs;
    
    public KanbanException(Errors erro, String ... msgs) {
        this.erro = erro;
        this.msgs = msgs;
    }
    
    public Errors getErro() {
        return erro;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String msg : msgs) {
            sb.append(msg).append(" ");
        }
        return sb.toString();
    }
}
