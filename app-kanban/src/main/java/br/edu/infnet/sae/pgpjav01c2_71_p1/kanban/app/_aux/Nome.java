
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app._aux;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;

/**
 *
 * @author Túlio, Roberval, Lucas, Rafael
 */
public class Nome implements Nomeavel {

    private final String nome;

    public Nome(String nome) {
        this.nome = nome;
        
        if (nome == null 
        		|| nome.length() < 3
        		|| nome.length() > 80
        		|| nome.matches("/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+$/")
        		|| nome.startsWith(" ")
        		|| nome.endsWith(" ")
        		|| nome.matches("[ ]{2,}") ) 
        	
        	throw new IllegalArgumentException();
    }
    
    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void setNome(String nome) {
        throw new UnsupportedOperationException("Not supported.");
    }
    
    @Override
    public long getId() {
        return nome.hashCode();
    }

    @Override
    public void setId(long id) {
        throw new UnsupportedOperationException("Not supported.");
    }
    
    @Override
    public int compareTo(Object t) {
        if (t == null || !(t instanceof Nome)) throw new IllegalArgumentException();
        return nome.compareTo(((Nome)t).getNome());
    }
    
}
