
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.domain;

import static java.lang.System.out;

/**
 * Implementação parcial
 * 
 * @author Túlio, Roberval, Lucas, Rafael
 */
public enum Commands {

	KANBAN_COMANDO_INEXISTENTE, KANBAN_HELP, KANBAN_CREATE, KANBAN_LIST, KANBAN_EXCLUDE, KANBAN_USER_CREATE, KANBAN_USER_ALTER, KANBAN_USER_LIST, TAREFA_CREATE, TAREFA_EXCLUDE, TAREFA_STATE, TAREFA_LIST, KANBAN_LOG_LIST;

	public static Commands select(String[] args) {
		if (args == null || args.length == 0) {
			return KANBAN_COMANDO_INEXISTENTE;
		}
		if (args[0].equals("--help")) {
			return KANBAN_HELP;
		}
		if (args[0].equals("--adm") && args[1].equals("--kanban-create")) {
			return KANBAN_CREATE;
		}
		if (args[0].equals("--adm") && args[1].equals("--kanban-list")) {
			return KANBAN_LIST;
		}
		if (args[0].equals("--adm") && args[1].equals("--kanban-exclude")) {
			return KANBAN_EXCLUDE;
		}
		if (args[0].equals("--kanban") && args[2].equals("--user-create") && args[4].equals("--cpf")) {
			return KANBAN_USER_CREATE;
		}
		if (args[0].equals("--kanban") && args[2].equals("--user-change") && args[4].equals("--cpf")) {
			return KANBAN_USER_ALTER;
		}
		if (args[0].equals("--kanban") && args[2].equals("--user-list") && args[3].equals("--cpf")) {
			return KANBAN_USER_LIST;
		}
		if (args[0].equals("--kanban") && args[2].equals("--task-create") && args[4].equals("--cpf")) {
			return TAREFA_CREATE;
		}
		if (args[0].equals("--kanban") && args[2].equals("--task-exclude") && args[4].equals("--cpf")) {
			return TAREFA_EXCLUDE;
		}
		if (args[0].equals("--kanban") && args[2].equals("--task") && args[4].equals("--task-state") && args[6].equals("--cpf")) {
			return TAREFA_STATE;
		}
		if (args[0].equals("--kanban") && args[2].equals("--task-list") && args[3].equals("--cpf")) {
			return TAREFA_LIST;
		}
		if (args[0].equals("--kanban") && args[2].equals("--log-list") && args[3].equals("--cpf")) {
			return KANBAN_LOG_LIST;
		}

		return KANBAN_COMANDO_INEXISTENTE;
	}
}
