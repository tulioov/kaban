package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model;

import static java.lang.System.out;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.KanbanException;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app._aux.Nome;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.domain.Errors;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.util.Log;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadoKanban;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadoTarefa;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.LogVO;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.PessoaFisica;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.StoreApi;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl.KanbanStoreService;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl.LogStoreService;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl.TarefaStoreService;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.impl.UsuarioStoreService;

/**
 * Implementação parcial
 * 
 * @author Túlio, Roberval, Lucas, Rafael
 */
public class Kanban implements Nomeavel {

	private final StoreApi<Nomeavel> kbnStore = new KanbanStoreService();
	private final StoreApi<PessoaFisica> usrStore = new UsuarioStoreService();
	private final StoreApi<Nomeavel> tskStore = new TarefaStoreService();
	private final StoreApi<LogVO> logStore = new LogStoreService();
	private final List<Nomeavel> kanbans = kbnStore.list();

	private Nome nome;

	public void persist() throws Exception {

		if (!existeNaLista(this.nome) && this.kanbans.size() < 100) {
			kbnStore.create(this);
			System.out.println("KANBAN CRIADO COM SUCESS");
		} else {
			throw new KanbanException(Errors.KANBAN_COM_MESMO_NOME);
		}

	}

	public void update() throws KanbanException {

		if (!existeNaLista(this.nome) && this.isAtivo()) {
			kbnStore.update(this, this.nome);
			out.println("KABAN ATUALIZADA COM SUCESS");
		} else {
			throw new KanbanException(Errors.KANBAN_NAO_ATUALIZADO);
		}
	}

	private boolean isMudancaEstadoValido(Kanban kbn, Tarefa tsk, EstadosTarefa statusNovo) {

		EstadosTarefa statusAntigo = ((EstadoTarefa) tskStore).getEstado(kbn, tsk);

		if (statusAntigo.getId() == 1 && statusNovo.getId() == 2)
			return true;
		if (statusAntigo.getId() == 2 && statusNovo.getId() != 1)
			return true;
		if (statusAntigo.getId() == 3 && statusNovo.getId() != 2)
			return true;

		return false;

	}

	public void setTskEstado(Kanban kbn, Tarefa tsk, EstadosTarefa status, Long cpf) throws KanbanException {

		List<PessoaFisica> usrs = usrStore.list(this);
		List<Nomeavel> tsks = tskStore.list(this);
		PessoaFisica pf = userPertenceAoKaban(cpf, usrs);
		LogVO log = null;
		Date date = new Date();

		log.setDataHora(date);
		log.setKanban(kbn);
		log.setTarefa(tsk);
		log.setTarefaEstado(status);
		log.setUsuario(pf);

		if (isTskPertenceAoKaban(tsk.getNome(), tsks) && pf != null && kbn.isAtivo()
				&& isMudancaEstadoValido(kbn, tsk, status)) {

			((EstadoTarefa) tskStore).setEstado(kbn, tsk, status);

			logStore.create(log, kbn);
			Log.createLog(tsk.getNome(), status.getValue(), pf.getNome(), String.valueOf(pf.getCPF()));
			out.println("TAREFA ATUALIZADA COM SUCESS");

		} else {
			throw new KanbanException(Errors.TAREFA_NAO_ATUALIZADA);
		}

		boolean todasProntas = true;

		for (Nomeavel tskk : tsks) {
			EstadosTarefa status2 = ((EstadoTarefa) tskStore).getEstado(kbn, tskk);
			if (status2.getId() != EstadosTarefa.PRONTA.getId())
				todasProntas = false;
		}

		if (todasProntas)
			kbn.setInativo();

	}

	public boolean existeNaLista(Nome nome) {
		boolean existeNaLista = false;
		for (Nomeavel k : kanbans) {
			if (k.getNome().equals(nome.getNome()))
				existeNaLista = true;
		}
		return existeNaLista;
	}

	@Override
	public String getNome() {
		return nome.getNome();
	}

	@Override
	public void setNome(String nome) {
		this.nome = new Nome(nome);
	}

	@Override
	public long getId() {
		return nome.getId();
	}

	@Override
	public void setId(long id) {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public int compareTo(Object t) {
		return nome.compareTo(t);
	}

	public boolean isAtivo() {
		if (existeNaLista(this.nome))
			return ((EstadoKanban) kbnStore).isAtivo(this);
		return false;
	}

	public void setInativo() throws KanbanException {
		if (existeNaLista(this.nome)) {
			((EstadoKanban) kbnStore).setInativo(this);
			out.println("KANBAN INATIVO COM SUCESS");
		} else {
			throw new KanbanException(Errors.KANBAN_INEXISTENTE);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		List<Nomeavel> sortedList = kanbans.stream().sorted(Comparator.comparing(Nomeavel::getNome))
				.collect(Collectors.toList());

		for (Nomeavel kbn : sortedList) {
			sb.append(kbn.getNome()).append(",").append(((EstadoKanban) kbnStore).isAtivo(kbn) ? "ativo" : "inativo")
					.append("\n");
		}
		return sb.toString();
	}

	public void criarUsuario(Usuario usr, Kanban kbn) throws KanbanException {

		List<PessoaFisica> usrs = usrStore.list(this);

		if (userPertenceAoKaban(usr.getCPF(), usrs) == null && usrs.size() < 25 && kbn.isAtivo()
				&& !usr.getNome().equals("adm"))
			usrStore.create(usr, this);
		else
			throw new KanbanException(Errors.USUARIO_NAO_CRIADO);

	}

	public void alterarUsuario(Usuario usr, Kanban kbn) throws KanbanException {

		List<PessoaFisica> usrs = usrStore.list(this);
		if (userPertenceAoKaban(usr.getCPF(), usrs) != null && kbn.isAtivo()) {
			usrStore.update(usr, kbn);
			out.println("USER ALTERADO COM SUCESS");
		} else {
			throw new KanbanException(Errors.USUARIO_NAO_ALTERADO);
		}
	}

	public String listarUsuarios(Usuario usr, Kanban kbn) throws KanbanException {

		List<PessoaFisica> usrs = usrStore.list(kbn);

		if (userPertenceAoKaban(usr.getCPF(), usrs) != null && kbn.isAtivo()) {

			StringBuilder sb = new StringBuilder();
			List<PessoaFisica> userListSorted = usrs.stream().sorted(Comparator.comparing(PessoaFisica::getNome))
					.collect(Collectors.toList());

			for (PessoaFisica user : userListSorted) {
				sb.append(new Usuario(user).toString()).append("\n");
			}
			return sb.toString();

		}

		throw new KanbanException(Errors.USUARIO_NOME_INVALIDO);

	}

	public void criarTsk(Tarefa tsk, Kanban kbn, Long cpf) throws KanbanException {

		List<PessoaFisica> usrs = usrStore.list(kbn);
		List<Nomeavel> tsks = tskStore.list(kbn);
		PessoaFisica pf = userPertenceAoKaban(cpf, usrs);
		LogVO log = null;
		Date date = new Date();

		log.setDataHora(date);
		log.setKanban(kbn);
		log.setTarefa(tsk);
		log.setTarefaEstado(EstadosTarefa.A_FAZER);
		log.setUsuario(pf);

		if (pf != null && tsks.size() < 50 && kbn.isAtivo() && !isTskPertenceAoKaban(tsk.getNome(), tsks)) {
			tskStore.create(tsk, kbn);
			Log.createLog(tsk.getNome(), EstadosTarefa.A_FAZER.getValue(), pf.getNome(), String.valueOf(pf.getCPF()));
			logStore.create(log, kbn);
			out.println("TASK CRIADO COM SUCESS");
		} else {
			throw new KanbanException(Errors.TAREFA_NAO_CRIADA);
		}
	}

	public String listarTarefas(Kanban kbn, Long cpf) {

		List<PessoaFisica> usrs = usrStore.list(kbn);

		if (kbn.isAtivo() && userPertenceAoKaban(cpf, usrs) != null) {

			StringBuilder sb = new StringBuilder();
			List<Nomeavel> tsks = tskStore.list(kbn);

			List<Nomeavel> sortedList = tsks.stream().sorted(Comparator.comparing(Nomeavel::getNome))
					.collect(Collectors.toList());

			for (Nomeavel tsk : sortedList) {
				sb.append(new Tarefa(tsk).toString()).append("\n");
			}
			return sb.toString();
		}
		return null;
	}

	public void listarLog(String kbnNome, String usrCPF) throws IOException {

		BufferedReader br = new BufferedReader(new FileReader("Log.csv"));
		while (br.ready()) {
			String linha = br.readLine();
			System.out.println(linha);
		}
		br.close();

	}

	public PessoaFisica userPertenceAoKaban(Long cpf, List<PessoaFisica> usrs) {

		for (PessoaFisica user : usrs) {
			if (cpf == (user.getCPF()))
				return user;
		}

		return null;
	}

	public boolean isTskPertenceAoKaban(String nomeTsk, List<Nomeavel> tsks) {

		boolean tskExiste = false;
		for (Nomeavel tsk : tsks) {
			if (tsk.getNome().equals(nomeTsk))
				tskExiste = true;
		}

		return tskExiste;
	}

}
