package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;

public class LogBd {

	private String nome_kanban;
	private String nome_tarefa;
	private EstadosTarefa estado;
	private String nome_usuario;
	private Long cpf;

	public LogBd(String nome_kanban, String nome_tarefa, EstadosTarefa estado, String nome_usuario, Long cpf) {
		super();
		this.nome_kanban = nome_kanban;
		this.nome_tarefa = nome_tarefa;
		this.estado = estado;
		this.nome_usuario = nome_usuario;
		this.cpf = cpf;
	}

	public String getNome_kanban() {
		return nome_kanban;
	}

	public void setNome_kanban(String nome_kanban) {
		this.nome_kanban = nome_kanban;
	}

	public String getNome_tarefa() {
		return nome_tarefa;
	}

	public void setNome_tarefa(String nome_tarefa) {
		this.nome_tarefa = nome_tarefa;
	}

	public EstadosTarefa getEstado() {
		return estado;
	}

	public void setEstado(EstadosTarefa estado) {
		this.estado = estado;
	}

	public String getNome_usuario() {
		return nome_usuario;
	}

	public void setNome_usuario(String nome_usuario) {
		this.nome_usuario = nome_usuario;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

}
