package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app._aux.Nome;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;

public class Tarefa implements Nomeavel {

	private Nome nome;
	// private String state;

	public Tarefa() {
		super();
	}

	public Tarefa(Nomeavel task) {
		nome = new Nome(task.getNome());
	}

	@Override
	public String getNome() {
		return nome.getNome();
	}

	@Override
	public void setNome(String nome) {
		this.nome = new Nome(nome);
	}

	@Override
	public long getId() {
		return nome.getId();
	}

	@Override
	public void setId(long id) {
		throw new UnsupportedOperationException("Not supported.");
	}

	@Override
	public int compareTo(Object t) {
		return nome.compareTo(t);
	}

	public String toString() {
		return nome.getNome();
	}

	public EstadosTarefa getStatus(String status) {

		EstadosTarefa es = EstadosTarefa.ERRO_DE_STATUS;

		switch (status.toLowerCase().trim()) {
		case "a_fazer":
			es = EstadosTarefa.A_FAZER;
			break;
		case "em_andamento":
			es = EstadosTarefa.EM_ANDAMENTO;
			break;
		case "em_impedimento":
			es = EstadosTarefa.EM_IMPEDIMENTO;
			break;
		case "pronta":
			es = EstadosTarefa.PRONTA;
			break;
		case "excluída":
			es = EstadosTarefa.CANCELADA;
			break;
		}
		return es;
	}

}
