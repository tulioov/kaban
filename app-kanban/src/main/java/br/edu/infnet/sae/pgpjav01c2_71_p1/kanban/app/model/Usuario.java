
package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.model;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.KanbanException;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app._aux.Nome;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.domain.Errors;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.util.Util;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.Nomeavel;
import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.PessoaFisica;

/**
 * A FAZER concluir regras
 * 
 * @author Túlio, Roberval, Lucas, Rafael
 */
public class Usuario implements PessoaFisica {
 
    private final Nome nome;
    private final long cpf;

    public Usuario(PessoaFisica usuario) throws KanbanException {
        nome = new Nome(usuario.getNome());
        cpf = usuario.getCPF();
    }
    
    public Usuario(Nome nome, String cpf) throws KanbanException {
    	try {
			if(!nome.equals("adm"))
				this.nome = nome;	
			else 
				throw new KanbanException(Errors.USUARIO_NOME_INVALIDO);
		} catch (IllegalArgumentException ex) {
			throw new KanbanException(Errors.USUARIO_NOME_INVALIDO);
		}
        try {
        	
            this.cpf = Long.parseLong(cpf);
            if (cpf.length() != 11) throw new NumberFormatException();
            if (!Util.cpfValido(this.cpf)) throw new KanbanException(Errors.USUARIO_CPF_INVALIDO);
        } catch (NumberFormatException ex) {
            throw new KanbanException(Errors.USUARIO_CPF_INVALIDO);
        }
    }
    
    /**
     * <p>Há ocasioes onde nome do usuário não importa pois este, pela interface, 
     * é identificado pelo CPF: usar diretamente a interface Identificavel
     * seria mais 'trabalhoso'.
     * 
     * @param cpf
     * @throws KanbanException 
     */
    public Usuario(String cpf) throws KanbanException {
        this.nome = new Nome("NaoImporta");
        try {
            this.cpf = Long.parseLong(cpf);
            if (cpf.length() != 11) throw new NumberFormatException();
            if (!Util.cpfValido(this.cpf)) throw new KanbanException(Errors.USUARIO_CPF_INVALIDO);
        } catch (NumberFormatException ex) {
            throw new KanbanException(Errors.USUARIO_CPF_INVALIDO);
        }
    }
    
    @Override
    public long getCPF() {
        return cpf;
    }

    @Override
    public void setCPF(long cpf) {
        throw new UnsupportedOperationException("Not supported");   
    }

    @Override
    public String getNome() {
        return nome.getNome();
    }

    @Override
    public void setNome(String nome) {
        throw new UnsupportedOperationException("Not supported"); 
    }

    @Override
    public long getId() {
        return getCPF();
    }

    @Override
    public void setId(long id) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public int compareTo(Object t) {
        if (t == null || !(t instanceof PessoaFisica)) throw new IllegalArgumentException();
        return nome.compareTo((Nomeavel)t);
    }
    
    @Override
    public String toString() {
        return nome.getNome() + "," + String.format("%011d", getCPF());
    }
    
}