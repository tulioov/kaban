package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.persistence.api.EstadosTarefa;

public class Log {
	
	private static final String arqLog = "Log.csv";

	public static void createLog(String tskName, String tskState, String userName, String usercpf) {
		try {
			
			File file = new File(arqLog);

            if (!file.exists() || getNumeroLinhas()>=1000) {
            	file.delete();
            	file.createNewFile();
            }
            
            add(tskName, tskState, userName, usercpf);
            
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void add(String tskName, String tskState, String userName, String usercpf) throws Exception {

		LocalDateTime agora = LocalDateTime.now();
		String dataFormatada = agora.format(DateTimeFormatter.ofPattern("dd/MM/yyyy,HH:mm:ss"));
		String saida = dataFormatada + "," + tskName + "," + tskState + "," + userName + "," + usercpf + ";";
		FileWriter fw = null;
		PrintWriter pw = null;

		try {
			File arquivo = new File(arqLog); 
			fw = new FileWriter(arquivo, true);
			pw = new PrintWriter(fw);

			pw.println(saida);

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			pw.close();
			fw.close();
		}
	}
		
	public static int getNumeroLinhas() throws IOException {
		
		LineNumberReader lnr = new LineNumberReader(new FileReader(new File(arqLog)));
		lnr.skip(Long.MAX_VALUE);
		int retorno = lnr.getLineNumber();
		lnr.close();
		
		return retorno;
	}

}
