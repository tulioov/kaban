package br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.util;

import java.util.Arrays;

public class Util {

	private static final int[] WEIGHTS_11 = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 },
			WEIGHTS_10 = Arrays.copyOfRange(WEIGHTS_11, 1, 10);
	private static final String PAT_D2 = "[0-9]{2}", PAT_D3 = "[0-9]{3}", PAT_SEP_1 = "([._]?)", PAT_SEP_2 = "[-/]",
			PAT_CPF = PAT_D3 + PAT_SEP_1 + PAT_D3 + "\\1" + PAT_D3 + PAT_SEP_2 + PAT_D2;
	private static final String[] INVALID_CPF = { "00000000000", "11111111111", "22222222222", "33333333333",
			"44444444444", "55555555555", "66666666666", "77777777777", "88888888888", "99999999999" };

	public static  boolean cpfValido(long cpf) {

		String scpf = Long.toString(cpf);

		for (String invalid : INVALID_CPF) {
			if (scpf.equals(invalid)) {
				return false;
			}
		}
		final String CPF_1_a_9 = scpf.substring(0, 9);
		final int CPF_10 = Integer.parseInt(scpf.substring(9, 10));
		final int CPF_11 = Integer.parseInt(scpf.substring(10, 11));
		final int DV_10 = calcDV(CPF_1_a_9, WEIGHTS_10);
		final int DV_11 = calcDV(CPF_1_a_9 + DV_10, WEIGHTS_11);
		return CPF_10 == DV_10 && CPF_11 == DV_11;
	}

	private static int calcDV(final String str, int[] weights) {
		int sum = innerProduct(digits(str), weights);
		sum = 11 - (sum % 11);
		return sum > 9 ? 0 : sum;
	}

	private static int[] digits(final String str) {
		String[] cdigits = str.split("");
		int[] digits = new int[str.length()];
		for (int i = 0; i < digits.length; i++) {
			digits[i] = Integer.parseInt(cdigits[i]);
		}
		return digits;
	}

	private static int innerProduct(int[] a, int[] b) {
		int ip = 0;
		if (a == null || b == null || a.length > b.length) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i < a.length; i++) {
			ip += a[i] * b[i];
		}
		return ip;
	}

}
