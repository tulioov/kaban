# Kanban

Implementação parcial do kanban para auxiliar no desenvolvimento do trabalho.

O propósito da organização deste blueprint é ilustrar como, na prática, precisamos 
de interagir com API de terceiros para realizar outras implementações sem "partir do zero".

A conveniência de implementarmos uma aplicação "na íntegra", só é comum durante a fase de aprendizado. 
As empresas têm a maioria dos sistemas (conjuntos de aplicações) "prontos" mas que necessitam de adaptação constante. 
Parte destes sistemas são "sistema legado" -- sistemas desatualizados em relação a plataformas "correntes" com alto custo de manutenção. 

Esse blueprint pode ser alterado ou mesmo ignorado (à conveniência dos alunos).

# maven

Para gerar este artefato:
```
mvn clean package
```

Para gerar a documentação deste artefato:
```
mvn site:site
```

Para instalar as dependências para o armazenamento no repositório maven:

```
mvn install:install-file -Dfile=app-kanban-persistence-api-1.0.0.jar \
  -DgroupId=br.edu.infnet.sae.pgpjav01c2_71_p1 -DartifactId=app-kanban-persistence-api \
  -Dversion=1.0.0 -Dpackaging=jar
mvn install:install-file -Dfile=app-kanban-persistence-impl-1.0.0.jar \
  -DgroupId=br.edu.infnet.sae.pgpjav01c2_71_p1 -DartifactId=app-kanban-persistence-impl \
  -Dversion=1.0.0 -Dpackaging=jar
```

Para copiar as dependências necessárias para a execução da app 
para um diretório ('lib' por ex.): 
```
mvn dependency:copy-dependencies -DoutputDirectory=lib
```

# Execução

Para executar o app-kanban a partir do diretório base do projeto com as dependências no diretório 'lib':
```
java -Xss64m -Xmx512m \
-cp lib/h2-1.4.200.jar:lib/app-kanban-persistence-impl-1.0.0.jar:lib/app-kanban-persistence-api-1.0.0.jar:target/app-kanban-1.0.0.jar \
br.edu.infnet.sae.pgpjav01c2_71_p1.kanban.app.AppKanban --help
```

# Armazenamento

A "camada" de persistência não é o "foco" do trabalho. 
Por isso há artefatos encarregados desta tarefa que atendem (com restrições mas de modo suficiente) 
aos requisitos de armazenamento.

O [H2 Database](https://h2database.com) é o banco de dados (relacional) empregado, operando em modo "embarcado".
O nome do banco de dados da aplicação é 'kanban.mv.db'. Se este for excluído será recriado novamente, sem informações.
A "lib" do H2 é necessária ao armazenamento.
  
# BUGS

É difícil garantir a ausência de erros. 
Estes artefatos formam testados (nas funcionalidades necessárias). Se houver erros estes serão corrigidos e 
uma nova versão dos artefatos será postada no moodle.

Há um BUG (mvn site:site) no plugin maven-jxr-plugin que produz parâmetros incorretos nos tipos genéricos dos códigos-fontes.