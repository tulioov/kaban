# A FAZER

- versão 1.0.1 de 2019-12-15

- esse ZIP contem os artefatos:
  1. app-kanban (projeto maven)
  2. app-kanban-persistence-api (projeto maven)
  3. app-kanban-persistence-api-1.0.0.jar e o respectivo javadoc
  4. app-kanban-persistence-impl-1.0.0.jar e o respectivo javadoc

- o app-kanban no diretório lib já contém as dependências para a execução da aplicação. 
  Contudo, para o 'build' do projeto as dependências (artefatos 3 e 4 acima) devem ser 
  instalados conforme explicado no trabalho

- esse readme deve ser alterado para incluir:
  1. as informações dos autores do trabalho
	Rafael Fitipald
	Roberval Ribeiro
	Túlio Vasconcellos
	Lucas Silva
  2. como efetuar a execução do app-kanban
	1. criar kanban: --adm --kanban-create ‘nome-do-kanban’ 
	2. excluir kanban: --adm --kanban-exclude ‘nome-do-kanban’ 
	3. listar de kanbans (saída: nome-do-kanban, estado [‘ativo’, ‘inativo’]): --adm --kanban-list 
	4. criar usuário no kanban: --kanban ‘nome-do-kanban’ --user-create ‘nome-do-usuário’ --cpf ‘número-do-cpf’ 
	5. alterar o nome usuário no kanban: --kanban ‘nome-do-kanban’ --user-change ‘nome-do-usuário’ --cpf ‘número-do-cpf’ 
	6. listar os usuários do kanban (saída: nome-do-usuário, cpf): --kanban ‘nome-do-kanban’ --user-list --cpf ‘número-do-cpf’ 
	7. criar uma tarefa no kanban: --kanban ‘nome-do-kanban’ --task-create ‘nome-da-tarefa’ --cpf ‘número-do-cpf’ 
	8. excluir uma tarefa no kanban: --kanban ‘nome-do-kanban’ --task-exclude ‘nome-da-tarefa’ --cpf ‘número-do-cpf’ 
	9. alterar o estado de uma tarefa 
	(onde o estado é ‘a_fazer’, ‘em_andamento’, ‘em_impedimento’, ‘pronta’): 
	--kanban ‘nome-do-kanban’ --task ‘nome-da-tarefa’ --task-state ‘estado’ --cpf ‘número-do-cpf’ 
	10. listar as tarefas e respectivos estados do kanban (saída: nome-da-tarefa, estado 
	[‘a_fazer’, ‘em_andamento’, ‘em_impedimento’, ‘pronta’, ‘excluída’]): --kanban ‘nome-do-kanban’ --task-list --cpf ‘número-do-cpf’ 
	11. listar o log do kanban: 
	(saída: data, hora, nome-da-tarefa, estado, nome-do-usuário, cpf) --kanban ‘nome-do-kanban’ --log-list --cpf ‘número-do-cpf’
	
  3. como efetuar o 'build' do app-kanban
	IDE do eclipse
	


